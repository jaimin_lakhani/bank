/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jaiminlakhani
 */
import java.io.*;

public class Bank {
    private String name;
    
    Bank(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
}
